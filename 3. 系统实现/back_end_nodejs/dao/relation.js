const Neo4j = require('node-neo4j')

// 批量添加前驱关系
const createPreRelation = ({ url }, nodeId, preNodeList) => {
  // 添加单个前驱关系
  const single = (neo, nodeId, preNode) => {
    return new Promise(resolve => {
      const cql = `
        MATCH (P), (N) WHERE ID(P)=${preNode.id} AND ID(N)=${nodeId}
        ${preNode.relationType.map(item => `MERGE (P)-[:${item}]->(N)`).join(' ')}
      `
      neo.cypherQuery(cql, (err, res) => {
        if (err) throw err
        resolve(res)
      })
    })
  }
  return new Promise(resolve => {
    const neo = new Neo4j(url)
    Promise.all(preNodeList.map(preNode => {
      return single(neo, nodeId, preNode)
    })).then(res => resolve(res))
  })
}

// 批量添加后继关系
const createBackRelation = ({ url }, nodeId, backNodeList) => {
  const single = (neo, nodeId, backNode) => {
    return new Promise(resolve => {
      const cql = `
        MATCH (N), (B) WHERE ID(N)=${nodeId} AND ID(B)=${backNode.id}
        ${backNode.relationType.map(item => `MERGE (N)-[:${item}]->(B)`).join(' ')}
      `
      neo.cypherQuery(cql, (err, res) => {
        if (err) throw err
        resolve(res)
      })
    })
  }
  return new Promise(resolve => {
    const neo = new Neo4j(url)
    Promise.all(backNodeList.map(backNode => {
      return single(neo, nodeId, backNode)
    })).then(res => resolve(res))
  })
}

// 根据节点id 获取关系列表
const getRelationListByNodeId = ({ url }, source, target) => {
  return new Promise(resolve => {
    const neo = new Neo4j(url)
    const cql = `MATCH (S)-[R]->(T) WHERE ID(S)=${source} AND ID(T)=${target} RETURN R, TYPE(R)`
    neo.cypherQuery(cql, (err, res) => {
      if (err) throw err
      const data = res.data.map(item => {
        return {
          id: item[0]._id,
          name: item[1]
        }
      })
      resolve(data)
    })
  })
}

// 根据id创建关系
const createRelationById = ({ url }, source, target, relationList) => {
  return new Promise(resolve => {
    const neo = new Neo4j(url)
    const cql = `
      MATCH (S), (T) WHERE ID(S)=${source} AND ID(T)=${target}
      ${relationList.map(item => `MERGE (S)-[:${item}]->(T)`).join(' ')}
    `
    neo.cypherQuery(cql, (err, res) => {
      if (err) {
        throw err
      }
      resolve(res)
    })
  })
}

// 根据ids删除关系
const deleteRelationList = ({ url }, ids) => {
  return new Promise(resolve => {
    const neo = new Neo4j(url)
    const cql = `MATCH ()-[R]-() WHERE ID(R) IN [${ids}] DELETE R`
    neo.cypherQuery(cql, (err, res) => {
      if (err) throw err
      resolve(res)
    })
  })
}

// 根据rid查询关系详情
const getRelationDetail = ({ url }, rid) => {
  return new Promise(resolve => {
    const neo = new Neo4j(url)
    const cql = `MATCH ()-[R]-() WHERE ID(R)=${rid} RETURN R`
    neo.cypherQuery(cql, (err, res) => {
      if (err) throw err
      resolve(res)
    })
  })
}

module.exports = {
  createPreRelation, // 批量添加前驱关系
  createBackRelation, // 批量添加后继关系
  getRelationListByNodeId, // 根据id查询关系列表
  createRelationById, // 根据id创建关系
  deleteRelationList, // 根据ids删除关系列表
}