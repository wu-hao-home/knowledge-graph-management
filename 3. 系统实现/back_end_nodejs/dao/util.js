const MongoClient = require('mongodb').MongoClient

const dbConf = {
  user: 'wuhao',
  pwd: '123456',
  ip: '106.52.115.64',
  port: '27017',
  database: 'kgd'
}

const url = `mongodb://${dbConf.user}:${dbConf.pwd}@${dbConf.ip}:${dbConf.port}/${dbConf.database}`

// 获取数据库对象
const connectMongoDB = () => {
  return new Promise((resolve, reject) => {
    MongoClient.connect(url, (err, client) => {
      if (err) {
        reject()
        throw err
      }
      const dbo = client.db(dbConf.database)
      resolve({ client, dbo })
    })
  })
}

// 关闭数据库
const closeMongoDB = (c) => c.close()

module.exports = {
  connectMongoDB, closeMongoDB
}