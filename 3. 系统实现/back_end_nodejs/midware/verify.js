/**
 * 验证用户权限
 */
const getRes = require('../util').getRes

module.exports = (res, resp, next) => {
  // 不需要验证的api直接过
  if (!res.path.includes('/token')) {
    next()
    return true
  }
  const role = res.cookies.role
  if (role === undefined || role === typeof undefined) { // 此用户没有登录
    const res = getRes('fail', {}, '没有访问权限')
    resp.send(res)
    return false
  }
  // 需要验证管理员身份的api
  if (res.path.includes('/admin') && role !== '超级管理员') {
    const res = getRes('fail', {}, '没有访问权限')
    resp.send(res)
    return false
  }
  next()
}