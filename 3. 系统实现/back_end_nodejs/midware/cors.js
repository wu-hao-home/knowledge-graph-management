/**
 * 处理跨域问题
 */

 let allowOrigins = [
  "http://localhost:8000"
]

module.exports = (request, response, next) => {
  // 处理简单请求
  if ("origin" in request.headers && allowOrigins.includes(request.headers.origin)) {
      response.header("access-control-allow-origin", request.headers.origin);
  }
  // 处理预检请求
  if (request.method === 'OPTIONS') {
      response.header("access-control-allow-method", request.headers["access-control-request-method"]);
      response.header("access-control-allow-headers", request.headers["access-control-request-headers"])
  }
  response.header("access-control-allow-credential", "true")
  next();
}