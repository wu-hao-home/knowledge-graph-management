const cors = () => require('./cors')
const verify = () => require('./verify')

module.exports = {
  cors, verify
}