// 获取响应结果
const getRes = (status, data, msg) => {
  return {
    status, data, msg
  }
}

// 批量设置cookie
const setCookies = (resp, cookies) => {
  for (let name in cookies) {
    resp.cookie(name, cookies[name], {
      httpOnly: true
    })
  }
}

module.exports = {
  getRes, setCookies
}