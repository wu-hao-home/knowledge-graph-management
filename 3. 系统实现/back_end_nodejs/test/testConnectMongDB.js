const MongoClient = require('mongodb').MongoClient

const dbConf = {
  user: 'wuhao',
  pwd: '123456',
  ip: '106.52.115.64',
  port: '27017',
  database: 'kgd'
}

const url = `mongodb://${dbConf.user}:${dbConf.pwd}@${dbConf.ip}:${dbConf.port}/${dbConf.database}`

MongoClient.connect(url, (err, client) => {
  if (err) {
    throw err
  }
  const dbo = client.db(dbConf.database)
  // dbo.collection('user').insertOne({
  //   username: '3238901193@qq.com',
  //   password: '123456',
  //   name: '吴浩',
  //   role: '超级管理员',
  //   cg: null,
  //   graphs: []
  // }).then(res => console.log(res))
  dbo.collection('user').find({}).toArray((_err, res) => {
    console.log(res)
    client.close()
  })
})