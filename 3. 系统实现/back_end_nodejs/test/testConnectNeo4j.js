const Neo4j = require("node-neo4j")

const dbConf = {
  account: 'neo4j',
  pass: '123',
  ip: '106.52.115.64',
  port: '7474'
}

const db = new Neo4j(`http://${dbConf.account}:${dbConf.pass}@${dbConf.ip}:${dbConf.port}`)

db.cypherQuery('MATCH (n:Person) WHERE n.name = "孙" RETURN n', (err, node) => {
  if (err) {
    throw err
  }
  console.log(node.data)
})
