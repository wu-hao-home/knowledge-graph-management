const dao = require('../dao').visual()
const getNeo4jFromMongoDB = require('../dao').graph().getNeo4jFromMongoDB
const getRes = require('../util').getRes

// 查询图谱数据
const select = async (username, keywords, level, limit) => {
  limit = parseInt(limit)
  const neo = await getNeo4jFromMongoDB(username)
  const res = await dao.select(neo, keywords, level, limit)
  return getRes(200, res, '请求成功')
}

// 查询节点详情
const getNodeInfo = async (username, nodeId) => {
  const neo = await getNeo4jFromMongoDB(username)
  const res = await dao.getNodeInfo(neo, nodeId)
  return getRes(200, res, '请求成功')
}

// 查询关系详情
const getRelaInfo = async (username, rid) => {
  const neo = await getNeo4jFromMongoDB(username)
  const res = await dao.getRelaInfo(neo, rid)
  return getRes(200, res, '请求成功')
}

module.exports = {
  select,
  getNodeInfo,
  getRelaInfo
}