const dao = require('../dao').relation()
const getNeo4jFromMongoDB = require('../dao').graph().getNeo4jFromMongoDB
const getRes = require('../util').getRes

// 根据source 与 target 获取之间的关系列表
const getRelationList = async (username, source, target) => {
  const neo = await getNeo4jFromMongoDB(username)
  const res = await dao.getRelationListByNodeId(neo, source, target)
  return getRes('success', res, '请求成功')
}

// 根据节点id 建立关系
const addRelation = async (username, source, target, relationList) => {
  const neo = await getNeo4jFromMongoDB(username)
  const res = await dao.createRelationById(neo, source, target, relationList)
  return getRes('success', res, '添加成功')
}

// 根据id删除关系列表
const deleteRelationList = async (username, ids) => {
  const neo = await getNeo4jFromMongoDB(username)
  const res = await dao.deleteRelationList(neo, ids)
  return getRes('success', res, '删除成功')
}

// 获取关系详情
const getRelationDetail = async (username, rid) => {
  const neo = await getNeo4jFromMongoDB(username)
  const res = await dao.getRelationDetail(neo, rid)
  return getRes(200, res, '查询成功')
}

module.exports = {
  getRelationList,
  addRelation,
  deleteRelationList,
  getRelationDetail
}