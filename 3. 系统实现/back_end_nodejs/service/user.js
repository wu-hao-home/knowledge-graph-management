const dao = require('../dao').user()
const getRes = require('../util').getRes

// 登录
const login = async (username, password) => {
  const data = await dao.login(username)
  if (!data || data.length === 0) {
    return getRes('fail', {}, '用户不存在')
  }
  const user = data[0]
  if (user.password !== password) {
    return getRes('fail', {}, '用户名或密码错误')
  }
  return getRes('success', user, '登录成功')
}

// 注销
const logout = async username => {
  await dao.logout(username)
  return getRes('success', {}, '注销成功')
}

module.exports = {
  login,
  logout
}