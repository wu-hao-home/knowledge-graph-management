const dao = require('../dao').graph()
const getRes = require('../util').getRes

// 连接时需要做的事情
// 1.同步类型
// 2.存储图谱连接信息
const connect = async (ip, account, pass, username) => {
  const neo = await dao.connect(ip, account, pass)
  // 图谱连接失败
  if (neo === undefined) {
    return getRes('fail', {}, '连接失败')
  }
  // 连接成功 存入MongoDB
  await dao.saveNeo4jToMongoDB(username, neo)
  // 请求知识图谱中节点类型数据
  const nodeTypes = await dao.getNodeTypesByNeo4j(neo)
  // 请求知识图谱中关系类型的数据
  const relationTypes = await dao.getRelationTypesByNeo4j(neo)
  // 将节点类型与关系类型同步到MongoDB中
  await dao.saveNodeAndRelationTypesToMongoDB(username, nodeTypes, relationTypes)
  return getRes('success', { ip }, '连接成功')
}

module.exports = {
  connect
}