const user = () => require("./user")
const graph = () => require("./graph")
const type = () => require("./type")
const node = () => require("./node")
const relation = () => require("./relation")
const visual = () => require('./visual')

module.exports = {
  user, graph, type, node, relation, visual
}