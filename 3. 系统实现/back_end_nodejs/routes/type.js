/**
 * 类型管理接口路由
 */
 const router = require("express").Router()
 const service = require("../service").type()

// 节点类型查询
router.get('/token/node', async (req, resp) => {
  const username = req.cookies.username
  const res = await service.getNodeTypes(username)
  resp.send(res)
})
// 节点类型查询 v2
router.get('/token/node/v2', async (req, resp) => {
  const username = req.cookies.username
  const res = await service.getNodeTypesV2(username)
  resp.send(res)
})
// 添加节点类型
router.post('/token/add-node', async (req, resp) => {
  const username = req.cookies.username
  const newType = req.body.type
  const res = await service.addNodeType(username, newType)
  resp.send(res)
})
// 根据类型获取节点数量
router.get('/token/node-count', async (req, resp) => {
  const username = req.cookies.username
  const nodeType = req.query.type
  const res = await service.getNodeCount(username, nodeType)
  resp.send(res)
})

// 关系类型查询
router.get('/token/relation', async (req, resp) => {
  const username = req.cookies.username
  const res = await service.getRelationTypes(username)
  resp.send(res)
})
// 关系类型查询 v2 解决了关系类型更细不及时的问题
router.get('/token/relation/v2', async (req, resp) => {
  const username = req.cookies.username
  const res = await service.getRelationTypesV2(username)
  resp.send(res)
})
// 添加关系类型
router.post('/token/add-relation', async (req, resp) => {
  const username = req.cookies.username
  const newType = req.body.type
  const res = await service.addRelationType(username, newType)
  resp.send(res)
})
// 根据类型获取关系数量
router.get('/token/relation-count', async (req, resp) => {
  const username = req.cookies.username
  const relationType = req.query.type
  const res = await service.getRelationCount(username, relationType)
  resp.send(res)
})


 module.exports = router