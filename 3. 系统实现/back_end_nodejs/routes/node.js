/**
 * 类型管理接口路由
 */
const router = require("express").Router()
const service = require("../service").node()

// 查询节点列表
// 页数，每页的条数，关键字(name)，类型
router.get('/token/list', async (req, resp) => {
  const username = req.cookies.username
  const page = req.query.page
  const records = req.query.records
  const keywords = req.query.keywords
  const type = req.query.type || ''
  const res = await service.getList(username, page, records, keywords, type)
  resp.send(res)
})

// 根据节点的id，查询该节点的信息及其一层关系网络
router.get('/token/info', async (req,resp) => {
  const username = req.cookies.username
  const nodeId = req.query.nodeId
  const res = await service.getNodeInfo(username, nodeId)
  resp.send(res)
})

// 根据节点id，修改节点信息
router.post('/token/update', async (req, resp) => {
  const username = req.cookies.username
  const nodeId = req.body.id
  const nodeName = req.body.name
  const nodeType = req.body.type
  const oldNodeType = req.body.oldType
  const res = await service.updateNodeInfo(username, nodeId, nodeName, nodeType, oldNodeType)
  resp.send(res)
})

// 新增节点
router.post('/token/add', async (req, resp) => {
  const username = req.cookies.username
  const nodeName = req.body.name
  const nodeType = req.body.type
  const preNodeList = req.body.prevNodeList
  const backNodeList = req.body.backNodeList
  const res = await service.addNode(username, nodeName, nodeType, preNodeList, backNodeList)
  resp.send(res)
})

// 删除节点
router.post('/token/del', async (req, resp) => {
  const username = req.cookies.username
  const ids = req.body.ids
  const res = await service.deleteNodeList(username, ids)
  resp.send(res)
})


 module.exports = router