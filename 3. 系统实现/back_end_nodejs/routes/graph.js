/**
 * 图谱接口路由
 */
const router = require("express").Router()
const service = require("../service").graph()

 // 连接知识图谱
router.post('/token/connect', async (req, resp) => {
  const { ip, account, pass } = req.body
  const username = req.cookies.username
  const res = await service.connect(ip, account, pass, username)
  resp.send(res)
})

 module.exports = router