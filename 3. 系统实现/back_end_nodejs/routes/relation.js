/**
 * 关系管理接口路由
 */
const router = require("express").Router()
const service = require("../service").relation()

// 根据源节点id 与 目标节点id 查询两个节点的所有关系
router.get('/token/list', async (req, resp) => {
  const username = req.cookies.username
  const source = req.query.source
  const target = req.query.target
  const res = await service.getRelationList(username, source, target)
  resp.send(res)
})

// 根据源节点id 与 目标节点id 创建两个节点的关系
router.post('/token/add', async (req, resp) => {
  const username = req.cookies.username
  const source = req.body.source
  const target = req.body.target
  const relationList = req.body.relationList
  const res = await service.addRelation(username, source, target, relationList)
  resp.send(res)
})

// 根据id数组删除关系列表
router.post('/token/del', async (req, resp) => {
  const username = req.cookies.username
  const ids = req.body.ids
  const res = await service.deleteRelationList(username, ids)
  resp.send(res)
})



module.exports = router