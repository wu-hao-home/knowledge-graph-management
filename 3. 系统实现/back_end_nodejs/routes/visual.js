/**
 * 可视化接口路由
 */

 const router = require("express").Router()
 const service = require("../service").visual()

// 查询
router.get('/token/select', async (req, resp) => {
  const { keywords, level, limit } = req.query
  const username = req.cookies.username
  const res = await service.select(username, keywords, level, limit)
  resp.send(res)
})

// 查询节点详情
router.get('/token/node', async (req, resp) => {
  const nodeId = req.query.nodeId
  const username = req.cookies.username
  const res = await service.getNodeInfo(username, nodeId)
  resp.send(res)
})

// 查询关系详情
router.get('/token/rela', async (req, resp) => {
  const rid = req.query.rid
  const username = req.cookies.username
  const res = await service.getRelaInfo(username, rid)
  resp.send(res)
})

 module.exports = router