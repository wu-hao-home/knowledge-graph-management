﻿const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const midware = require('./midware')
const routes = require('./routes')

// 创建应用
const app = express()

// 处理跨域中间件
app.use(midware.cors())

// 使用cookie
app.use(cookieParser())

// 处理消息体中间件
app.use(express.json())

// 验证权限中间件
app.use(midware.verify())

// 注册静态资源处理中间件
app.use(express.static(path.resolve(__dirname, './static')))

// 注册路由接口
app.use('/api/user', routes.user())
app.use('/api/graph', routes.graph())
app.use('/api/type', routes.type())
app.use('/api/node', routes.node())
app.use('/api/relation', routes.relation())
app.use('/api/visual', routes.visual())


app.listen(8000, () => console.log('服务器已监听8000端口...'))