package com.example.mapper;

import com.example.pojo.Student;
import com.example.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface LoginMapper {
    public void save();
    public void saveUser(String role, String username, String name, String password, Map<String,String> graphs[]);
    public User queryUserByName(String username);
}
