package com.example.MapperImpl;

import com.example.mapper.LoginMapper;
import com.example.pojo.Student;
import com.example.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class LoginMapperImpl implements LoginMapper {
    @Autowired
    MongoTemplate mongoTemplate;

    public void save() {
        mongoTemplate.save(new Student("123","张三",11,36.9,new Date()));
    }

    @Override
    public void saveUser(String role,String username, String name, String password, Map<String, String>[] graphs) {
        mongoTemplate.insert(new User(role,username,name,password,graphs));
    }




    public User queryUserByName(String username) {
        Query query= new Query(Criteria.where("username").is(username));
        List<User> users = mongoTemplate.find(query, User.class);
        if(users.size()>0){
            return users.get(0);
        }
        return null;

    }
}
