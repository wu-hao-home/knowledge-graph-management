package com.example.Config;

import com.example.intercepter.AdminInterceptor;
import com.example.intercepter.ApiInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ApiInterceptor()).addPathPatterns("/api/**");
        registry.addInterceptor(new AdminInterceptor()).addPathPatterns("/admin/**");
    }
}
