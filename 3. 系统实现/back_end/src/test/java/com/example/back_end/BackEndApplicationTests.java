package com.example.back_end;

import com.example.MapperImpl.LoginMapperImpl;
import com.example.pojo.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class BackEndApplicationTests {

    @Autowired
    LoginMapperImpl loginMapper;

    @Test
    void test(){
        loginMapper.save();
    }

    @Test
    void test2(){
        HashMap<String, String> graphs[] = new HashMap[1];
        HashMap<String, String> map = new HashMap<>();
        map.put("url","8.3.5.125");
        map.put("account","neo4j");
        map.put("pass","123");
        graphs[0] = map;
        loginMapper.saveUser("超级管理员","1295713045@qq.com","李四","123456",graphs);
    }

}
