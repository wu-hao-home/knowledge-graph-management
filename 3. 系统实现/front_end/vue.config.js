module.exports = {
  publicPath: "/",
  productionSourceMap: false,
  devServer: {
    proxy: {
      '/api/': {
        // target: 'http://127.0.0.1:8000',
        // target: 'http://1.15.8.2',
        // target: 'http://www.flycat.cloud',
        target: 'http://127.0.0.1:8000',
        changeOrigin: true
      }
    }
  },
  lintOnSave: false
}