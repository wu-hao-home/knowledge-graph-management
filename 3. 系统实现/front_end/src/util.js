import Vue from 'vue'
import router from './router'
import store from './store'

const axios = require('axios')

// 生成随机数[0 ~ 10]
export const random = () => {
  return (Math.random() * 10).toFixed(2)
}

// 返回一个请求对象
export const request = () => {
  // 统一错误处理 403
  axios.interceptors.response.use(resp => {
    if (resp.data && resp.data.status === 'fail' && resp.data.msg === '没有访问权限') {
      Vue.prototype.$message.warning(resp.data.msg)
      store.dispatch('logoutService').then(res => {
        router.push({ path: '/home' })
      })
      return false
    }
    return resp
  })
  return axios
}

// 深度克隆
export const clone = o => {
  // 判断如果不是引用类型，直接返回数据即可
  if (typeof o === 'string' || typeof o === 'number' || typeof o === 'boolean' || typeof o === 'undefined') {
    return o
  } else if (Array.isArray(o)) { // 如果是数组，则定义一个新数组，完成复制后返回
    var _arr = []
    o.forEach(item => { _arr.push(item) })
    return _arr
  } else if (typeof o === 'object') {
    var _o = {}
    for (const key in o) {
      _o[key] = clone(o[key])
    }
    return _o
  }
}

// 截取字符串
export const getLimitText = (text, number) => {
  return text.length > number ? text.slice(0, number) + '...' : text
}
