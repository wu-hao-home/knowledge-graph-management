import Vue from 'vue'
import VueRouter from 'vue-router'
import DefaultLayout from '../layouts/DefaultLayout.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: DefaultLayout,
    redirect: '/home',
    children: [
      {
        path: '/home',
        name: 'home',
        component: () => import('@/views/Home.vue'),
        meta: {
          title: '首页'
        }
      },
      {
        path: '/node-manage',
        name: 'node-manage',
        component: () => import('@/views/NodeManage.vue'),
        meta: {
          title: '节点管理'
        }
      },
      {
        path: '/type-manage',
        name: 'type-manage',
        component: () => import('@/views/TypeManage.vue'),
        meta: {
          title: '类型管理'
        }
      },
      {
        path: '/relation-manage',
        name: 'relation-manage',
        component: () => import('@/views/RelationManage.vue'),
        meta: {
          title: '关系管理'
        }
      },
      {
        path: '/visualization-manage',
        name: 'visualization-manage',
        component: () => import('@/views/VisualizationManage.vue'),
        meta: {
          title: '可视化查询'
        }
      },
      {
        path: '/admin-manage',
        name: 'admin-manage',
        component: () => import('@/views/AdminManage.vue'),
        meta: {
          title: '管理员管理'
        }
      },
      {
        path: '/demo',
        name: 'demo',
        component: () => import('@/views/Demo.vue')
      }
    ]
  },
  {
    path: '*',
    name: 'Error',
    component: () => import('@/views/404'),
    meta: {
      title: '404 - not found'
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
