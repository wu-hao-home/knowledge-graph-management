import Vue from 'vue'
import {
  BootstrapVue,
  IconsPlugin,
  TooltipPlugin
} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(TooltipPlugin)
