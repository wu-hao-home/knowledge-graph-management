import Vue from 'vue'
import * as echarts from 'echarts/lib/echarts'
import 'echarts/lib/component/title'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/grid'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/toolbox'

import 'echarts/lib/chart/graph'
import 'echarts/lib/chart/effectScatter'

Vue.prototype.echarts = echarts
