import Vue from 'vue'
import {
  Loading,
  Message,
  Select,
  Option,
  Icon,
  Table,
  TableColumn,
  Input,
  Pagination,
  Button,
  MessageBox,
  Form,
  FormItem,
  Row,
  Col,
  Dialog
} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(Select)
Vue.use(Option)
Vue.use(Icon)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Input)
Vue.use(Pagination)
Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Col)
Vue.use(Row)
Vue.use(Dialog)

Vue.prototype.$loading = Loading.service
Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm
