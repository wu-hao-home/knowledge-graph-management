import { request } from '../util'

export const login = async (username, password) => {
  const resp = await request().post('/api/user/login', { username, password })
  return resp.data
}

export const logout = async () => {
  const resp = await request().get('/api/user/token/logout')
  return resp.data
}
