import { request } from '../util'

// 连接知识图谱
export const connectGraph = async (ip, account, pass) => {
  const resp = await request().post('/api/graph/token/connect', {
    ip, account, pass
  })
  return resp.data
}
