import { request } from '../util'

// 根据id 获取 关系列表
export const getRelationList = async params => {
  const resp = await request().get(`/api/relation/token/list?source=${params.source}&target=${params.target}`)
  return resp.data
}

// 根据id添加关系列表
export const addRelationList = async ({ source, target, relationList }) => {
  const resp = await request().post('/api/relation/token/add', {
    source, target, relationList
  })
  return resp.data
}

// 根据ids删除关系
export const delRelationList = async ({ ids }) => {
  const resp = await request().post('/api/relation/token/del', { ids })
  return resp.data
}
