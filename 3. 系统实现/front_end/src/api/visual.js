// 可视化接口
import { request } from '../util'

// 可视化查询接口
export const getVisualData = async params => {
  const resp = await request().get(`/api/visual/token/select?keywords=${params.keywords}&level=${params.level}&limit=${params.limit}`)
  return resp.data
}

// 获取节点详情
export const getNodeInfo = async params => {
  const resp = await request().get(`/api/visual/token/node?nodeId=${params.nodeId}`)
  return resp.data
}

// 获取关系详情
export const getRelaInfo = async params => {
  const resp = await request().get(`/api/visual/token/rela?rid=${params.rid}`)
  return resp.data
}