import { request } from '../util'

// 获取节点类型
export const getNodeTypes = async () => {
  const resp = await request().get('/api/type/token/node')
  return resp.data
}
// 获取节点类型v2
export const getNodeTypesV2 = async () => {
  const resp = await request().get('/api/type/token/node/v2')
  return resp.data
}

// 新增节点类型
export const addNodeType = async newNodeType => {
  const resp = await request().post('/api/type/token/add-node', {
    type: newNodeType
  })
  return resp.data
}

// 根据类型获取节点数量
export const getNodeCountByType = async nodeType => {
  const resp = await request().get(`/api/type/token/node-count?type=${nodeType}`)
  return resp.data
}

// 获取关系类型
export const getRelationTypes = async () => {
  const resp = await request().get('/api/type/token/relation')
  return resp.data
}

// 获取关系类 v2
export const getRelationTypesV2 = async () => {
  const resp = await request().get('/api/type/token/relation/v2')
  return resp.data
}

// 新增关系类型
export const addRelationType = async newRelationType => {
  const resp = await request().post('/api/type/token/add-relation', {
    type: newRelationType
  })
  return resp.data
}

// 根据类型获取关系数量
export const getRelationCountByType = async relationType => {
  const resp = await request().get(`/api/type/token/relation-count?type=${relationType}`)
  return resp.data
}
