import { request } from '../util'

// 获取节点列表
export const getNodeList = async ({ page, records, keywords, type }) => {
  const resp = await request().get(`
    /api/node/token/list?page=${page}&records=${records}&keywords=${keywords}&type=${type}
  `)
  return resp.data
}

// 根据id获取节点信息
export const getNodeInfo = async ({ nodeId }) => {
  const resp = await request().get(`/api/node/token/info?nodeId=${nodeId}`)
  return resp.data
}

// 根据id修改节点信息
export const updateNodeInfo = async ({ id, name, type, oldType }) => {
  const resp = await request().post('/api/node/token/update', {
    id, name, type, oldType
  })
  return resp.data
}

// 增加节点
export const addNode = async ({ name, type, prevNodeList, backNodeList }) => {
  const resp = await request().post('/api/node/token/add', {
    name, type, prevNodeList, backNodeList
  })
  return resp.data
}

// 删除节点列表
export const deleteNodeList = async ({ ids }) => {
  const resp = await request().post('/api/node/token/del', {
    ids
  })
  return resp.data
}
