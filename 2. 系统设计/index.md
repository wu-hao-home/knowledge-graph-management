# 知识图谱管理系统 系统设计说明书

## 1 引言
### 1.1 编写目的
以技术的角度去诠释“知识图谱管理系统”的实现，通过文档，技术人员或者相关对技术了解的客户或者工作人员可以清晰看到系统实现的逻辑，便于开发人员进行理解。
### 1.2 项目背景
我所选的毕设题目为《基于知识图谱的旅游推荐系统》，那么首先我们必须要做的事情就是构建一个旅游领域的知识图谱，我打算结合脚本构建与人工构建的方式，但直接使用CQL语句进行人工构建未免显得太过繁琐，为此我打算开发一个知识图谱管理系统，旨在为构建知识图谱提供一些便捷，同时也可以作为旅游推荐系统的后台管理系统。
### 1.3 需求概述
希望能够通过界面化操作，来进行知识图谱的构建，主要包括节点、关系、属性的CRUD操作。同时，该系统能够对构建好的知识图谱有一个数据展示的功能。
### 1.4 使用的文字处理与绘图工具
- 文字处理：markdown, wps
- 绘图工具：puml, processOn
## 2 模块设计
### 2.1 用例图设计
```puml
@startuml
left to right direction
actor 管理员 as m
package 知识图谱管理系统 {
  usecase "标签/类型管理" as uc1
  usecase "节点管理" as uc2
  usecase "关系管理" as uc3
  usecase "可视化展示" as uc4
  usecase "管理员管理" as uc5
}
actor 超级管理员 as sm
sm --> uc1
sm --> uc2
sm --> uc3
sm --> uc4
sm --> uc5
m --> uc1
m --> uc2
m --> uc3
m --> uc4
@enduml
```
### 2.2 功能模块设计(含接口设计)
#### 2.2.1 用户模块
##### 2.2.1.1 用户登录
- 请求地址：/api/user/login
- 请求方式：post 
- 请求参数

参数名|参数类型|字段
--|--|--
用户名|string|username
密码|string|password
- 请求成功示例
```js
const res = {
  status: 'success',
  data: {
    role: '超级管理员',
    username: '3238901193@qq.com',
    name: '吴浩',
    graphs: [
      { url: '8.3.5.125', account: 'neo4j', pass: '123' }
    ]
  },
  msg: '登录成功'
}
```
```puml
@startuml
  start
  partition 登录流程图 {
    :获取用户名、密码;
    note right: controller 层
    :对用户名、密码进行数据校验(这一步不做，默认合法);
    note right: service 层
    :根据用户名获取该用户所有信息;
    note right: 这一步调用DAO层查询
    if (是否能获取到该用户) then (是)

      if (比对数据库中的密码与输入密码是否一致) then (是)
        :登录成功;
        note right: { \n\tstatus: 'success', \n\tdata: { ... }, \n\tmsg: '登录成功' \n}
      else (否)
        :登录失败;
        note right: { \n\tstatus: 'fail', \n\tdata: {}, \n\tmsg: '用户名或密码错误' \n}
      endif

    else (否)
      :登录失败;
      note right: { \n\tstatus: 'fail', \n\tdata: {}, \n\tmsg: '该用户不存在' \n}
    endif
    :接收登录结果;
    note right: controller 层接收service 层返回的数据
    if (判断是否登录成功) then (是)
      :颁发通行证，用户可以进行相应权限的操作;
      note right: 这一步其实就是设置cookie(包括内容与时长)，\n内容应为用户id或者用户账号经过base64编码\n后进行对称加密的结果
    endif 
    :将接收的数据转成json写回客户端;
  }
  stop
@enduml
```
##### 2.2.1.2 用户注销
- 请求地址：/api/user/token/logout
- 请求方式：get
- 请求参数：无

- 请求成功示例
```js
const res = {
  status: 'success',
  data: {},
  msg: '注销成功'
}
```
```puml
@startuml
  start
  partition 注销流程图 {
    :验证token，获取身份信息;
    :清除知识图谱连接信息;
    :清除token;
    :注销成功;
  }
  stop
@enduml
```

##### 2.2.1.3 连接知识图谱
##### 2.2.2.1 知识图谱连接
- 请求地址：/api/graph/connect
- 请求方式：post
- 请求参数

参数名|参数类型|字段
--|--|--
URL|string|url
账号|string|account
密码|string|pass
- 请求成功示例
```js
  const res = {
    status: 'success',
    data: {
      url: '8.2.5.135',
      account: 'neo4j',
      pass: '123'
    },
    msg: '连接成功'
  }
```
```puml
@startuml
start
  partition 知识图谱连接流程图 {
    :获取身份信息;
    if (验证是否登录) then (是)
      :获取知识图谱的地址、账号和密码，进行连接;
      if (判断是否连接成功) then (是)
        :将连接信息存入MongoDB;
        :同步节点类型数据到MongoDB;
        :同步关系类型数据到MongDB;
        :连接成功，返回知识图谱IP地址;
      else (否)
        :连接失败，IP、账号或密码错误;
      endif
    else (否)
      :连接失败，请先进行登录;
    endif
  }
stop
@enduml
```

#### 2.2.2 类型管理模块

##### 2.2.2.1 节点类型(标签)查询
- 请求地址：/api/type/token/node
- 请求方式：get
- 请求参数：无
- 请求成功示例
```js
  const res = {
    status: 'success',
    data: [
      '壮族自治区',
      '景点',
      '古镇'
    ],
    msg: '查询成功'
  }
```
##### 2.2.2.2 关系类型查询
- 请求地址：/api/type/token/relation
- 请求方式：get
- 请求参数：无
- 请求成功示例
```js
  const res = {
    status: 'success',
    data: [
      '位于',
      '描述',
      '文化中心'
    ],
    msg: '查询成功'
  }
```
##### 2.2.2.4 添加节点类型(标签)
- 请求地址：/api/type/token/add-node
- 请求方式：post
- 请求参数：

参数名|参数类型|字段
--|--|--
节点类型|string|type
- 请求成功示例
```js
  const res = {
    status: 'success',
    data: {},
    msg: '添加成功'
  }
```
##### 2.2.2.5 添加关系类型
- 请求地址：/api/type/token/add-relation
- 请求方式：post
- 请求参数：

参数名|参数类型|字段
--|--|--
关系类型|string|type
- 请求成功示例
```js
  const res = {
    status: 'success',
    data: {},
    msg: '添加成功'
  }
```
#### 2.2.3 节点管理模块
##### 2.2.3.1 节点列表查询
- 请求地址：/api/node/token/list
- 请求方式：get
- 请求参数：

参数名|参数类型|字段
--|--|--
页码|number|page
每页条数|number|records
关键词|string|keywords
类型限制|string|type
- 请求成功示例
```js
  const res = {
    status: 'success',
    data: {
      total: 1,
      nodeList: [
        {
          name: '桂林',
          _id: 20,
          labels: [
            '旅游城市',
            '城市',
            '市级行政区'
          ]
        }
      ]
    },
    msg: '请求成功'
  }
```

##### 2.2.3.2 节点关系网络查询
- 请求地址：/api/node/token/info
- 请求方式：get
- 请求参数：

参数名|参数类型|字段
--|--|--
节点id|string|nodeId

- 请求成功示例
```js
const res = {
  "status": "success",
  "data": {
      "prevNodeList": [],
      "backNodeList": [
          {
              "relation": {
                  "id": 0,
                  "type": "西北部"
              },
              "backNode": {
                  "id": 41,
                  "name": "坡月村",
                  "type": [
                      "乡村"
                  ]
              }
          }
      ],
      "curNode": {
          "id": 100,
          "name": "巴马县",
          "type": [
              "县级行政区",
              "瑶族自治县"
          ]
      }
  },
  "msg": "请求成功"
}
```
##### 2.2.3.3 修改节点信息
- 请求地址：/api/node/token/update
- 请求方式：post
- 请求参数：

参数名|参数类型|字段
--|--|--
节点ID|number|id
节点名|string|name
节点新标签(类型)|array|type
节点旧标签(类型)|array|oldType
- 请求成功示例
```js
  const res = {
    status: 'success',
    data: {},
    msg: '修改成功'
  }
```
##### 2.2.3.4 添加节点
- 请求地址：/api/node/token/add
- 请求方式：post
- 请求参数：

参数名|参数类型|字段
--|--|--
节点名|string|name
节点标签(类型)|array\<string\>|type
前驱节点(可选)|array|prevNodeList
后继节点(可选)|array|backNodeList
- 请求成功示例
```js
  const res = {
    status: 'success',
    data: {},
    msg: '添加成功'
  }
```

#### 2.2.4 关系管理模块
##### 2.2.4.1 关系列表查询
- 请求地址：/api/relation/token/list
- 请求方式：get
- 请求参数：

参数名|参数类型|字段
--|--|--
源节点ID|number|source
目标节点ID|number|target
- 请求成功示例
```js
const res = {
  "status": "success",
  "data": [
      {
          "id": 99,
          "name": "管辖"
      }
  ],
  "msg": "请求成功"
}
```

##### 2.2.4.2 添加关系
- 请求地址：/api/relation/token/add
- 请求方式：post
- 请求参数：

参数名|参数类型|字段
--|--|--
源节点ID|number|source
目标节点ID|number|target
关系列表|array|relationList
- 请求成功示例
```js
const res = {
  "status": "success",
  "data": [],
  "msg": "添加成功"
}
```

##### 2.2.4.3 删除关系
- 请求地址：/api/relation/token/del
- 请求方式：post
- 请求参数：

参数名|参数类型|字段
--|--|--
删除关系的ID列表|array|ids
- 请求成功示例
```js
const res = {
  "status": "success",
  "data": [],
  "msg": "删除成功"
}
```


## 3 数据库设计
#### 3.1 用户集合(user collection)
```js
user = {
  username: '3238901193@qq.com',
  password: '123456',
  name: '吴浩',
  role: '超级管理员',
  graphs: [
    {
      ip: 'string',
      account: 'string',
      pass: 'string'
    }
  ],
  cg: {
    connector: new Neo4j(),
    nodeTypes: ['string', 'string', 'string'],
    relationTypes: ['string', 'string', 'string']
  }
}
```

## 4 系统安全性设计
此系统不对外使用，仅为内部人员构建知识图谱提供一定的方便，因此不过多考虑系统安全性

## 5 系统性能设计
- 使用预加载技术对浏览器所需要的资源进行提前加载
- 使用懒加载技术减少请求的次数以及请求的数量
- 使用防抖、节流等技术减少不必要的请求

## 6 系统出错处理设计
- 当用户请求的页面不存在时，返回404出错页面给予明确提示
- 当用户请求的接口不存在或者出错时，返回对应的错误信息，并采用element-ui组件库给予人性化提示